//app.js
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');

var MongoClient = require('mongodb').MongoClient;
var mongoUrl = 'mongodb://127.0.0.1:27017/dramas';
var _db;

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(express.static('dist'));

MongoClient.connect(mongoUrl, function (err, db) {
  if(err) {
    console.error(err);
    return;
  }

  console.log('connected to mongo');
  _db = db;
  app.listen(8888, function () {
    console.log('server is running...');
  });
});

app.all("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  if (req.method == 'OPTIONS') {
    res.send(200);
  } else {
    next();
  }
});

//app.js
var ObjectID = require('mongodb').ObjectID


//1.新增
app.post('/create', function(req, res, next) {
  var collection = _db.collection('my_dramas');

  //接收前端发送的字段,不存在则返回错误信息；存在则插入
  var dramas = req.body;
  if(!dramas.title || !dramas.imgurl || !dramas.type | !dramas.star) {
    res.send({errcode:-1,errmsg:"params missed"});
    return;
  }
  collection.insert({title: dramas.title,imgurl:dramas.imgurl,type:dramas.type,star:dramas.star}, function (err, ret) {
    if(err) {
      console.error(err);
      res.status(500).end();
    } else {
      res.send({errcode:0,errmsg:"ok"});
    }
  });
});


//2.获取列表
app.get('/dramas-entries/:type', function(req, res, next) {
  var collection = _db.collection('my_dramas');

  var dramasType = req.params.type;

  collection.find({type:dramasType}).sort({'star':-1}).toArray(function (err, ret) {
    if(err) {
      console.error(err);
      return;
    }
    res.json(ret);
  });
});

//3.删除
app.delete('/delete/:id', function (req, res, next) {
  var _id = req.params.id;
  var collection = _db.collection('my_dramas');
  console.log(_id)
  //使用mongodb的唯一ObjectId字段查找出对应id删除记录
  collection.remove({_id: new ObjectID(_id)} ,function (err, result) {
    if(err) {
      console.error(err);
      res.status(500).end();
    } else {
      res.send({errcode:0,errmsg:"ok"});
    }
  });
});

//4.更新
app.post('/addtoviewed', function (req, res, next) {
  var oldone = req.body;
  var _id = oldone._id;
  var _title = oldone._title;
  var _imgurl = oldone._imgurl;
  var _star = oldone._star;
  var collection = _db.collection('my_dramas');

  collection.update({_id: new ObjectID(_id)} ,{type:'viewed',title:_title,imgurl:_imgurl,star:_star});
});

app.post('/addtowanted', function (req, res, next) {
  var oldone = req.body;
  var _id = oldone._id;
  var _title = oldone._title;
  var _imgurl = oldone._imgurl;
  var _star = oldone._star;
  var collection = _db.collection('my_dramas');

  collection.update({_id: new ObjectID(_id)} ,{type:'wanted',title:_title,imgurl:_imgurl,star:_star});
});

app.post('/update',function (req,res,next) {
  var oldone = req.body;
  var _id = oldone.id;
  var _title = oldone.title;
  var _imgurl = oldone.imgurl;
  var _type = oldone.type;
  var _star = oldone.star;

  var collection = _db.collection('my_dramas');

  collection.update({_id: new ObjectID(_id)} ,{type:_type,title:_title,imgurl:_imgurl,star:_star});
})







