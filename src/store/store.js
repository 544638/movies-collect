import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  dramasEntries: [],
  dramasEntry: {
    id: '',
    title: '',
    star:1,
    imgurl: '',
    type: '',
    edit: ''
  },
  dramasIndex: '',
  dialogFormVisible: false
}

const mutations = {
  addDramas(state, dramasEntry){
    state.dramasEntries.push(dramasEntry)
  },

  fetchData (state, data){
    state.dramasEntries = data
  },

  deleteDramas(state, dramasEntry){
    let index = state.dramasEntries.indexOf(dramasEntry)
    state.dramasEntries.splice(index, 1)
  },

  getIndex(state, dramasEntry){
    let index = state.dramasEntries.indexOf(dramasEntry)
    state.dramasIndex = index;
  },

  updateDramas(state, updateInfo){
    let index = updateInfo.index,data = updateInfo.data;
    state.dramasEntries.splice(index,1,data)
  },

  resetValue(state, data){
    state.dramasEntry = data
  },

  dialogState(state, dstate){
    state.dialogFormVisible = dstate;
  },

}

export default new Vuex.Store({
  state,
  mutations
})
