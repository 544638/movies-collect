import Vue from 'vue'
import App from './App'

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(ElementUI)

import store from './store/store'


import Wanted from './components/Wanted'
import Viewed from './components/Viewed'
import Addup from './components/Addup'
const routes = [
  {path:'/wanted',component:Wanted},
  {path:'/viewed',component:Viewed},
  {path:'',component:Wanted}
]

const router = new VueRouter({
  routes
})

/*挂载App*/
new Vue({
  el: '#app',
  template: '<App/>',
  store,
  router:router,
  components: { App }
})


